import axios from 'axios';
export default async function getResult(param){
    // const data = await fetch(`https://api.mathjs.org/v4/?expr=${param}`)
    // return data.json()
    const response = await axios.get(`https://api.mathjs.org/v4/?expr=${ param }`)
    return response.data
}