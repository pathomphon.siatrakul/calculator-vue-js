import { createStore } from "vuex";

export default createStore({
  state: {
    loadingStatus: 'notLoading',
    dataLocalStorage:[],
    dataLocal: JSON.parse(localStorage.getItem("historyList")),
  },
  getter:{
    doneDataLocalStorage(state){
      return state.dataLocalStorage
    }
  },
  mutations: {
    SET_LOADING_STATUS(state, status){
      state.loadingStatus = status
    },
    SET_DATA_LOCAL_STORAGE(state, dataLocalStorage){
      state.dataLocalStorage = dataLocalStorage
    }
  },
  actions: {
    fetchDataLocalStorage(context){
      context.commit('SET_LOADING_STATUS', 'loading')
      let data = JSON.parse(localStorage.getItem("historyList"));
      context.commit('SET_LOADING_STATUS', 'notLoading')
      context.commit('SET_DATA_LOCAL_STORAGE', data)
    }
  },
  modules: {},
});
