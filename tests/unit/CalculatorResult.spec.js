import { shallowMount, mount } from "@vue/test-utils";
import CalculatorResult from "../../src/components/CalculatorResult.vue";
import { createStore } from "vuex";

describe("CalculatorResult", () => {
  beforeAll(() => {
    global.confirm = jest.fn(() => true);
  });
  const store = createStore({
    state() {
      return {
        dataLocal: [
          {
            name: "Calculator A",
            result: 123.4,
            formula: "123 + 0.4",
            date: "30/7/2021-17:47.34"
          },
          {
            name: "Calculator B",
            result: 12,
            formula: "6 x 2",
            date: "6/8/2021-13:8.3"
          }
        ]
      }
    }
  });

  const wrapper = mount(CalculatorResult, {
    global: {
      plugins: [store]
    },
  });

  it("Display logs data", () => {
    expect(wrapper.find('.calName').text()).toBe("Calculator A");
    expect(wrapper.find('.result').text()).toBe("123.4");
    expect(wrapper.find('.formula').text()).toBe("123 + 0.4");
    expect(wrapper.find('.Date').text()).toBe("30/7/2021-17:47.34");
  });

  it("Test function onClear", async () => {
    await wrapper.find(".clear").trigger("click");
    expect(global.confirm).toHaveBeenCalledWith(
      expect.any(String),
    );
    expect(wrapper.find('.show-text-when-null').text()).toBe("There's no history yet");
  });

});
