import { shallowMount, mount  } from "@vue/test-utils";
import Calculator from "../../src/components/Calculator.vue";
import getResult from "../../src/services/service"
import axios from 'axios';

// global.fetch = jest.fn().mockImplementation(() =>
//   Promise.resolve({
//     json: () => 10,
//   })
// );
axios.get = jest.fn().mockImplementation(() =>
  Promise.resolve({
    data: 10,
  })
);

describe("Calculator.vue", () => {
  describe("Test showing numbers.", () => {
    it("Show number 5 + 5 ", () => {
    const formula = "5 + 5";
    const result = "10";
    const wrapper = shallowMount(Calculator, {
        data() {
            return {
              resultScreenText: result,
              formulaScreenText: formula,
            };
          },
    });
    expect(wrapper.find('.formula').text()).toBe(formula);
    expect(wrapper.find('.result').text()).toBe(result);
    });
  });

  describe("Test conditions", () => {
    it("If you press 0 after pressing the plus operator and then press 5, formula = 5 + 5", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".plus").trigger("click");
      await wrapper.find(".zero").trigger("click");
      await wrapper.find(".five").trigger("click");
      expect(wrapper.find('.formula').text()).toBe('5 + 5');
    });
    it("If you press 0 after pressing the minus operator and then press 5, formula = 5 - 5", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".minus").trigger("click");
      await wrapper.find(".zero").trigger("click");
      await wrapper.find(".five").trigger("click");
      expect(wrapper.find('.formula').text()).toBe('5 - 5');
    });
    it("If you press 0 after pressing the multiply operator and then press 5, formula = 5 x 5", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".multiply").trigger("click");
      await wrapper.find(".zero").trigger("click");
      await wrapper.find(".five").trigger("click");
      expect(wrapper.find('.formula').text()).toBe('5 x 5');
    });
    it("Replace the multiplication operator with a plus", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".multiply").trigger("click");
      await wrapper.find(".plus").trigger("click");
      expect(wrapper.find('.formula').text()).toBe('5 +');
    });
    it("Replace the dot operator with a plus", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".dot").trigger("click");
      await wrapper.find(".plus").trigger("click");
      expect(wrapper.find('.formula').text()).toBe('5 +');
    });
    it("Replace the dot operator with a dot", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".plus").trigger("click");
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".dot").trigger("click");
      await wrapper.find(".dot").trigger("click");
      expect(wrapper.find('.formula').text()).toBe('5 + 5.');
    });
  });
  
  describe("Test +, -, x, Result and clear", () => {
    it("add number 5 + 5", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".plus").trigger("click");
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".equal").trigger("click");
      const result = await getResult(wrapper.find('.formula').text());
      expect(result).toEqual(10);
      expect(wrapper.find('.formula').text()).toBe('5 + 5');
    });
    it("minus number 20 - 10", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".two").trigger("click");
      await wrapper.find(".zero").trigger("click");
      await wrapper.find(".minus").trigger("click");
      await wrapper.find(".one").trigger("click");
      await wrapper.find(".zero").trigger("click");
      await wrapper.find(".equal").trigger("click");
      const result = await getResult(wrapper.find('.formula').text());
      expect(result).toEqual(10);
      expect(wrapper.find('.formula').text()).toBe('20 - 10');
    });
    it("multiply number 5 x 2", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".five").trigger("click");
      await wrapper.find(".multiply").trigger("click");
      await wrapper.find(".two").trigger("click");
      await wrapper.find(".equal").trigger("click");
      const result = await getResult(wrapper.find('.formula').text());
      expect(result).toEqual(10);
      expect(wrapper.find('.formula').text()).toBe('5 x 2');
    });
    it("clear number", async () => {
      const wrapper = mount(Calculator);
      await wrapper.find(".clear").trigger("click");
      expect(wrapper.find('.result').text()).toBe("0");
      expect(wrapper.find('.formula').text()).toBe("0");
    });
  });

  // describe("Test event after press equal", () => {
  
  //   it("add number 5 + 5", async () => {
  //     const wrapper = mount(Calculator);
  //     await wrapper.find(".five").trigger("click");
  //     await wrapper.find(".plus").trigger("click");
  //     await wrapper.find(".five").trigger("click");
  //     await wrapper.find(".equal").trigger("click");
  //     const result = await getResult(wrapper.find('.formula').text());
  //     expect(result).toEqual(10);
  //     expect(wrapper.find('.formula').text()).toBe('5 + 5');
  //   });
  // });
});
